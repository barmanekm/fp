package org.fp.safetypes;

import java.util.Objects;

// "Value" types representing quantities
final class Price {
    final double value;
    Price(double value) {
        this.value = value;
    }

    Price add(Price that) {
        return new Price(this.value + that.value);
    }

    Price mult(int count) {
        return new Price(this.value * count);
    }

    static final Price ZERO = new Price(0.0);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Double.compare(price.value, value) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }
}
