package org.fp.safetypes;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;


public class SafeTypesDemo {


    @Test
    void safeShopTest() {

        Product toothPaste = new Product("Tooth paste", new Price(1.5), new Weight(0.5));
        Product toothBrush = new Product("Tooth brush", new Price(3.5), new Weight(0.3));
        List<OrderLine> order = Arrays.asList(new OrderLine(toothPaste, 2), new OrderLine(toothBrush, 3));

        Weight weight = order.stream().map(OrderLine::getWeight).reduce(Weight.ZERO, Weight::add);
        Price price = order.stream().map(OrderLine::getAmount).reduce(Price.ZERO, Price::add);

        //Weight weight1 = order.stream().map(OrderLine::getAmount).reduce(Price.ZERO, Price::add);

        System.out.println(String.format("Total price: %s", price.value));
        System.out.println(String.format("Total weight: %s", weight.value));
    }

}
