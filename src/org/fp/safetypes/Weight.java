package org.fp.safetypes;

import java.util.Objects;

final class Weight {
    final double value;

    Weight(double value) {
        this.value = value;
    }

    Weight add(Weight that) {
        return new Weight(this.value + that.value);
    }

    Weight mult(int count) {
        return new Weight(this.value * count);
    }

    static final Weight ZERO = new Weight(0.0);


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weight weight = (Weight) o;
        return Double.compare(weight.value, value) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }
}
