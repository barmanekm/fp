package org.fp.simpleprogram;

import io.vavr.Tuple2;
import io.vavr.control.Option;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.util.Optional;

public class AbstractReader implements Input {
    protected final BufferedReader reader;

    protected AbstractReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public Try<Tuple2<Option<String>, Input>> readString() {
        try {
            String s = reader.readLine();
            return s.length() == 0
                    ? Try.success(new Tuple2<>(Option.none(), this))
                    : Try.success(new Tuple2<>(Option.some(s), this));
        } catch (Exception e) {
            return Try.failure(e);
        }
    }

    @Override
    public Try<Tuple2<Option<Integer>, Input>> readInt() {
        try {
            String s = reader.readLine();
            return s.length() == 0
                    ? Try.success(new Tuple2<>(Option.none(), this))
                    : Try.success(new Tuple2<>(Option.some(Integer.parseInt(s)), this));
        } catch (Exception e) {
            return Try.failure(e);
        }
    }
}
