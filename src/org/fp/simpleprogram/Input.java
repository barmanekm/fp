package org.fp.simpleprogram;

import io.vavr.Tuple2;
import io.vavr.control.Option;
import io.vavr.control.Try;

public interface Input {
    Try<Tuple2<Option<String>, Input>> readString();

    Try<Tuple2<Option<Integer>, Input>> readInt();

    default Try<Tuple2<Option<String>, Input>> readString(String message) {
        return readString();
    }

    default Try<Tuple2<Option<Integer>, Input>> readInt(String message) {
        return readInt();
    }
}
