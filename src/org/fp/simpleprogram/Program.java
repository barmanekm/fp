//package org.fp.simpleprogram;
//
//import io.vavr.Function1;
//import io.vavr.Tuple2;
//import io.vavr.control.Option;
//import io.vavr.control.Try;
//import org.junit.jupiter.api.Test;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.function.Consumer;
//import java.util.function.Supplier;
//
//import static org.fp.simpleprogram.ConsoleReader.consoleReader;
//
//
//
//public class Program {
//
//    //Output effects
//    static Consumer<String> printToConsole = System.out::println;
//
//    //Input
//    static Supplier<Try<Option<String>>> readString = () -> {
//        try {
//            String s = new BufferedReader(new InputStreamReader(System.in)).readLine();
//            return Try.success(s.length() == 0 ? Option.none() : Option.some(s));
//        } catch (IOException e) {
//            return Try.failure(e);
//        }
//    };
//
//    static Try<Option<String>> promptForString(String message) {
//        printToConsole.accept(message);
//        return readString.get();
//    }
//
//    static Supplier<Try<Option<Integer>>> readInt = () -> {
//        try {
//            String s = new BufferedReader(new InputStreamReader(System.in)).readLine();
//            return Try.success(s.length() == 0 ? Option.none() : Option.some(Integer.parseInt(s)));
//        } catch (IOException e) {
//            return Try.failure(e);
//        }
//    };
//
//    static Try<Option<Integer>> promptForInt(String message) {
//        printToConsole.accept(message);
//        return readInt.get();
//    };
//
//    // External services
//    private Try<Boolean> authorize(String username, Integer pin) {
//
//    }
//
//
//    @Test
//    public void run() {
//
//        Try<Option<Try<Boolean>>> options = promptForString("Enter username").flatMap(usernameOpt ->
//                promptForInt("Enter pin").map(pinOpt ->
//                        usernameOpt.flatMap(username ->
//                                pinOpt.map(pin ->
//                                        authorize(username, pin)
//                                )
//                        )
//                )
//        );
//
//    }
//
//
//
//}
