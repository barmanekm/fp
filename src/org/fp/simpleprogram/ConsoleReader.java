package org.fp.simpleprogram;

import io.vavr.Tuple2;
import io.vavr.control.Option;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConsoleReader extends AbstractReader {
    protected ConsoleReader(BufferedReader reader) {
        super(reader);
    }

    @Override
    public Try<Tuple2<Option<String>, Input>> readString(String message) {
        System.out.print(message + " ");
        return readString();
    }

    @Override
    public Try<Tuple2<Option<Integer>, Input>> readInt(String message) {
        System.out.print(message + " ");
        return readInt();
    }

    public static ConsoleReader consoleReader() {
        return new ConsoleReader(new BufferedReader(new InputStreamReader(System.in)));
    }
}
