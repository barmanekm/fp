package org.fp.DonutExample;

import org.junit.jupiter.api.Test;
import java.util.List;

import static org.fp.DonutExample.DonutShopB.*;
import static org.junit.jupiter.api.Assertions.*;

class DonutShopBTest {

    @Test
    void shouldReturnPaymentForMultipleDonuts() {

        CreditCard creditCard = new CreditCard();
        Tuple<List<Donut>, Payment> purchase = buyDonuts(5, creditCard);

        assertEquals(5, purchase._1.size());
        assertEquals(10, purchase._2.amount);
        assertEquals(creditCard, purchase._2.creditCard);
    }
}