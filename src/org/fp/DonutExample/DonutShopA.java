package org.fp.DonutExample;

public class DonutShopA {

    public static Tuple<Donut, Payment> buyDonut(CreditCard creditCard) {
        return new Tuple<>(new Donut(), new Payment(creditCard, Donut.price));
    }


    public static class Payment {
        public final CreditCard creditCard;
        public final int amount;

        public Payment(CreditCard creditCard, int amount) {
            this.creditCard = creditCard;
            this.amount = amount;
        }
    }

    public static class CreditCard {
        public void charge(int price) {
            // Charge the credit card
        }
    }

    public static class Donut {
        public static final int price = 2;
    }

    public static class Tuple<T, U> {
        public final T _1;
        public final U _2;
        public Tuple(T t, U u) {
            this._1 = t;
            this._2 = u;
        }
    }
}
