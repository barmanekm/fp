package org.fp.DonutExample;

public class DonutShop {
    public static Donut buyDonut(CreditCard creditCard) {
        Donut donut = new Donut();
        creditCard.charge(Donut.price);
        return donut;
    }

    public class CreditCard {
        private int total;
        void charge(int price) {
            this.total += price;
            // charge credit card
        }
        public int getTotal() {
            return total;
        }
    }

    public static class Donut {
        public static final int price = 2;
    }

}
