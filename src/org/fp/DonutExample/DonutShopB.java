package org.fp.DonutExample;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;


public class DonutShopB {

    public static Tuple<Donut, Payment> buyDonut(CreditCard creditCard) {
        return new Tuple<>(new Donut(), new Payment(creditCard, Donut.price));
    }

    public static Tuple<List<Donut>, Payment> buyDonuts(final int quantity,
                                                        final CreditCard creditCard) {
        return new Tuple<>(Collections.nCopies(quantity, new Donut()),
                new Payment(creditCard, Donut.price * quantity));
    }

    public static List<Optional<Payment>> groupByCard(List<Payment> payments) {
        return payments
                .stream()
                .collect(groupingBy(x -> x.creditCard))
                .values()
                .stream()
                .map(x -> x.stream().reduce(Payment::combinePayments))
                .collect(Collectors.toList());
    }

    static class Payment {
        final CreditCard creditCard;
        final int amount;

        Payment(CreditCard creditCard, int amount) {
            this.creditCard = creditCard;
            this.amount = amount;
        }

        static Payment combinePayments(Payment p1, Payment p2) {
            if (p1.creditCard.equals(p2.creditCard)) {
                return new Payment(p1.creditCard, p1.amount + p2.amount);
            } else {
                throw new IllegalStateException("Cards don't match.");
            }
        }

//        public Payment combine(Payment payment) {
//            if (creditCard.equals(payment.creditCard)) {
//                return new Payment(creditCard, amount + payment.amount);
//            } else {
//                throw new IllegalStateException("Cards don't match.");
//            }
//        }

        Payment combine(Payment payment) {
            return combinePayments(this, payment);
        }
    }

    public static class Donut {
        public static final int price = 2;
    }

    public static class CreditCard {
        public void charge(int price) {
            // Charge the credit card
        }
    }

    public static class Tuple<T, U> {
        public final T _1;
        public final U _2;

        public Tuple(T t, U u) {
            this._1 = t;
            this._2 = u;
        }
    }
}
