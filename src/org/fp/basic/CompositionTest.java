package org.fp.basic;

import org.junit.jupiter.api.Test;
import java.util.function.Function;
import static org.junit.jupiter.api.Assertions.*;


class CompositionTest {

    @Test
    void compositionDemo1() {

        Function<Integer, Integer> times10 = number -> number * 10;
        Function<Integer, Integer> increment = number -> number + 1;

        Function<Integer, Integer> times10AndIncrementA = times10.andThen(increment);
        Function<Integer, Integer> times10AndIncrementB = increment.compose(times10);

        assertEquals(21, times10AndIncrementA.apply(2).intValue());
        assertEquals(21, times10AndIncrementB.apply(2).intValue());
    }

    @Test
    void compositionDemo2() {

        Function<String, String> trim = String::trim;
        Function<String, String> uppercase = String::toUpperCase;
        Function<String, String> lodash = x -> "___" + x + "___";

        Function<String, String> decorate = trim
                .andThen(uppercase)
                .andThen(lodash);

        assertEquals("___HELLO WORLD___", decorate.apply("Hello World"));
    }

    @Test
    void functionCurryingDemo1() {

        Function<Integer, Function<Integer, Integer>> add = x -> y -> x + y;
        Function<Integer, Integer> add5 = add.apply(5);

        assertEquals(10, add5.apply(5).intValue());
        assertEquals(5, add.apply(2).apply(3).intValue());
    }

    @Test
    void functionCurryingDemo2() {

            Function<String, Function<String, Function<Double, Function<Double, String>>>> conversion =
                    fromUnit -> toUnit -> factor -> input -> {
            Double result = input * factor;
            return input + " " + fromUnit + " is " + result + " " + toUnit;
        };

        Function<Double, String> toMiles = conversion.apply("km").apply("miles").apply(1.00/1.60936);
        Function<Double, String> toKm = conversion.apply("miles").apply("km").apply(1.60936);
        Function<Double, String> toPound = conversion.apply("kg").apply("pound").apply(1.00/0.4546);
        Function<Double, String> toKg = conversion.apply("pound").apply("kg").apply(0.4546);

        System.out.println(toMiles.apply(10.00));
        System.out.println(toKm.apply(10.00));
        System.out.println(toPound.apply(10.00));
        System.out.println(toKg.apply(10.00));
    }

}