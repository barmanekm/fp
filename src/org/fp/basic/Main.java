package org.fp.basic;

import java.util.function.Function;

public class Main {

    private static double divide(double a, double b) {
        return a / b;
    }

    //static functional method
    private static int times10(int number) {
        return number * 10;
    }

    private static int times5(int number) {
        number = 5;
        return number * number;
    }

    // function
    private static Function<Integer, Integer> times10func = new Function<Integer, Integer>() {
        @Override
        public Integer apply(Integer number) {
            return number * 10;
        }
    };

    // function (lambda expr)
    private static Function<Integer, Integer> times10lambda = number -> number * 10;
    private static Function<Integer, Integer> increment = number -> number + 1;

    public static void main(String[] args) {
//        System.out.println(divide(3, 3));
//
//        System.out.println(times5(1));
//        System.out.println(times5(5));
//
//        // method invoke (application)
//        System.out.println(times10(3));
        // function application
        System.out.println(times10func.apply(1));
        System.out.println(times10lambda.apply(1));
        System.out.println(increment.apply(1));

        //function composition
        Function<Integer, Integer> composedFunctions1 = increment.andThen(times10lambda);
        Function<Integer, Integer> composedFunctions2 = times10lambda.compose(increment);

        System.out.println(composedFunctions1.apply(1));
    }

    public class TaxApplicator1 {
        private int percent1 = 5;
        public void setPercent1(int percent1) {
            this.percent1 = percent1;
        }

        private int applyTax1(int price) {
            return price / 100 * (100 + percent1);
        }
    }

    public class TaxApplicator2 {
        public int percent2 = 7;

        private int applyTax2(int price) {
            return price / 100 * (100 + percent2);
        }

    }

    public class TaxApplicator3 {
        public final int percent3 = 9;

        private int applyTax3(int price) {
            return price / 100 * (100 + percent3);
        }
    }
}
