package org.fp.basic;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ImperativeToDeclarativeTest {

    final class Customer {
        private final String name;

        String getName() {
            return name;
        }

        private final String country;

        String getCountry() {
            return country;
        }

        Customer(String name, String country) {
            this.name = name;
            this.country = country;
        }
    }

    @Test
    void customerImperativeDemo() {

        List<Customer> customers = Arrays.asList(
                new Customer("Mike", "PL"),
                new Customer("Ann", "US"),
                new Customer("Mark", "UK"),
                new Customer("Ashley", "UK")
        );


        for (Customer customer : customers) {
            if (customer.getCountry().equals("UK")) {
                System.out.println(customer);
            }
        }

    }

    @Test
    void customerDeclarativeDemo() {

        List<Customer> customers = Arrays.asList(
                new Customer("Mike", "PL"),
                new Customer("Ann", "US"),
                new Customer("Mark", "UK"),
                new Customer("Ashley", "UK")
        );


        customers.stream()
                .filter(x -> x.getCountry().equals("UK"))
                .forEach(System.out::println);
    }
}
