package org.fp.basic;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class MonoidTest {

    static BinaryOperator<Integer> add = (x, y) -> x + y;
    static BiFunction<Integer, Integer, Integer> multiply = (x, y) -> x * y;
    static BinaryOperator<String> concat = String::concat;
    static BinaryOperator<List<?>> listConcat = (x, y) -> Stream.of(x, y)
            .flatMap(List::stream)
            .collect(Collectors.toList());

    @Test
    void associativityTests() {
        assertEquals(add.apply(add.apply(1, 2), 3), add.apply(1, add.apply(2, 3)));
        assertEquals(multiply.apply(multiply.apply(1, 2), 3), multiply.apply(1, multiply.apply(2, 3)));
        assertEquals(concat.apply(concat.apply("a", "b"), "c"), concat.apply("a", concat.apply("b", "c")));

        List<String> list1 = Arrays.asList("element1", "element2");
        List<String> list2 = Arrays.asList("element3");
        List<String> list3 = Arrays.asList("element4", "element5");

        assertEquals(listConcat.apply(list1, listConcat.apply(list2, list3)),
                listConcat.apply(listConcat.apply(list1, list2), list3));
    }

    @Test
    void identityTests() {
        assertEquals(add.apply(1, 0).intValue(), 1);
        assertEquals(multiply.apply(2, 1).intValue(), 2);
        assertEquals(concat.apply("Hello", ""), "Hello");
        List<Integer> list = Arrays.asList(1, 2);
        assertEquals(listConcat.apply(list, new ArrayList<Integer>()), list);
    }

    @Test
    void doubleAssociativity() {
        assertEquals((0.1 + 0.2) + 0.3, 0.1 + (0.2 + 0.3));
    }

    @Test
    void reduction() {

        Stream<Integer> numbers123 = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);


        Stream<Integer> numbers1 = Stream.of(1, 2, 3);
        Stream<Integer> numbers2 = Stream.of(4, 5, 6);
        Stream<Integer> numbers3 = Stream.of(7, 8, 9);

        Integer sumAll = numbers123.reduce(0, add);

        Integer sum1 = numbers1.reduce(0, add);
        Integer sum2 = numbers2.reduce(0, add);
        Integer sum3 = numbers3.reduce(0, add);

        assertEquals(sumAll, Stream.of(sum1, sum2, sum3).reduce(0, add));
    }

}
