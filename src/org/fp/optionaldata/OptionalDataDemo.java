package org.fp.optionaldata;


import io.vavr.control.Option;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;


public class OptionalDataDemo {


    static Function<List<Integer>, Integer> sum = xs -> {
        if (xs == null) {
            throw new IllegalArgumentException("Null instead of list passed as argument");
            //return null;
            //return Double.NaN; (in this case it is quite correct)
        }

        return xs.stream().mapToInt(x -> x).sum();
    };


    static Function<Integer, Integer> square = x -> {
        if (x == null) {
            //return null;
            //throw new IllegalArgumentException("Empty list passed as argument");
            //return Double.NaN;
        }

        return x * x;
    };



    public static Integer sumSquareDoubled(List<Integer> numbers) {
        return square.apply(sum.apply(numbers)) * 2;
        //return sum.andThen(x -> x * x).apply(numbers) * 2;
        //return sum.andThen(square).apply(numbers) * 2;
    }











    // Using optional
    static Function<List<Integer>, Optional<Integer>> sumOptional = xs -> {
        if (xs == null) {
            return Optional.empty();
        } else {
            return Optional.of(xs.stream().mapToInt(x -> x).sum());
        }
    };

    public static Optional<Integer> sumSquareDoubledOptional(List<Integer> numbers) {

        // Method chaining without a risk
        return sumOptional.apply(numbers)
                .map(x -> x * x)
                .map(x -> x * 2);
    }


    //Most often it is not required to use isPresent() etc., decision should be made just before system boundary.
    public static Function<Optional<Integer>, Optional<Integer>> overuseSquare = x -> {

        if (x.isPresent()) {
            Integer xVal = x.get();
            return Optional.of(xVal * xVal);
        }
        return Optional.empty();

        //return x.map(xx -> xx * xx);
    };





    // Using VAVr option
    static Function<List<Integer>, Option<Integer>> sumOption = xs -> {
        if (xs == null) {
            return Option.none();
        } else {
            return Option.of(xs.stream().mapToInt(x -> x).sum());
        }
    };

    public static Option<Integer> sumSquareDoubledOption(List<Integer> numbers) {

        // Method chaining without a risk
        return sumOption.apply(numbers)
                .map(x -> x * x)
                .map(x -> x * 2);
    }



}
