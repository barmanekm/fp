package org.fp.optionaldata;

import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.fp.optionaldata.OptionalDataDemo.*;
import static org.junit.jupiter.api.Assertions.*;

class OptionalDataDemoTest {

    List<Integer> numbers = Arrays.asList(0, 1, 2, 3, 4);
    List<Integer> emptyListOfNumbers = new ArrayList<>();
    List<Integer> missingNumbers = null;

    @Test
    void sumSquareDoubledTest() {
        assertEquals(Integer.valueOf(200), sumSquareDoubled(numbers));

        assertEquals(Integer.valueOf(0), sumSquareDoubled(emptyListOfNumbers));

        assertThrows(IllegalArgumentException.class, () -> sumSquareDoubled(missingNumbers));

        // Need to check missing numbers before or deal with exception try {....}
        //System.console().printf(sumSquareDoubled(missingNumbers).toString());
    }


    @Test
    void optionalSumSquareDoubledTest() {
        assertEquals(Integer.valueOf(200), sumSquareDoubledOptional(numbers).get());

        assertEquals(Integer.valueOf(0), sumSquareDoubledOptional(emptyListOfNumbers).get());

        assertEquals(false, sumSquareDoubledOptional(missingNumbers).isPresent());


        // Return something else when value is missing
        assertEquals(Integer.valueOf(0), sumSquareDoubledOptional(missingNumbers).orElse(0));


        // Get or Throw (if we want to)
        assertEquals(Integer.valueOf(200),
                sumSquareDoubledOptional(numbers)
                        .orElseThrow(IllegalArgumentException::new));

        assertThrows(IllegalArgumentException.class,
                () -> sumSquareDoubledOptional(missingNumbers)
                        .orElseThrow(IllegalArgumentException::new));


        // Perform side effect when value Present
        sumSquareDoubledOptional(missingNumbers)
                .ifPresent(x -> System.out.println(x.toString())); // no chaining possible

        sumSquareDoubledOptional(numbers)
                .ifPresent(x -> System.out.println(x.toString()));
    }


    @Test
    void optionSumSquareDoubledTest() {
        assertEquals(Integer.valueOf(200), sumSquareDoubledOption(numbers).get());

        assertEquals(Integer.valueOf(0), sumSquareDoubledOption(emptyListOfNumbers).get());

        assertEquals(false, sumSquareDoubledOption(missingNumbers).isDefined());

        // Return something else when value is missing
        assertEquals(Integer.valueOf(0), sumSquareDoubledOptional(missingNumbers).orElse(0));

        // Get or Throw
        assertEquals(Integer.valueOf(200),
                sumSquareDoubledOption(numbers)
                        .getOrElseThrow(IllegalArgumentException::new));

        assertThrows(IllegalArgumentException.class,
                () -> sumSquareDoubledOption(missingNumbers)
                        .getOrElseThrow(IllegalArgumentException::new));


        // Perform side effect when value Present
        sumSquareDoubledOption(missingNumbers)
                .peek(x -> System.out.println(x.toString()))
                .onEmpty(() -> System.out.println("Missing data!"));

        sumSquareDoubledOption(numbers)
                .peek(x -> System.out.println(x.toString()));
    }

    @Test
    void vavrOptionIsIterableTest() {

        //VAVr option is iterable
        //VAVr functional list implementation

        io.vavr.collection.List<Integer> results = io.vavr.collection.List.empty();

        io.vavr.collection.List<Integer> moreResults =
                results.appendAll(sumSquareDoubledOption(numbers))
                        .appendAll(sumSquareDoubledOption(missingNumbers))
                        .appendAll(sumSquareDoubledOption(Arrays.asList(0, 1, 2)));

        assertEquals(2, moreResults.length());
        assertEquals(218, moreResults.sum().intValue());
    }


    @Test
    void vavrVsJavaImportantDifference() {

        // Option.map(x -> null) -> empty
        assertEquals("No name!", Optional.of("Name")
                .map(s -> (String) null)
                .map(String::toLowerCase)
                .orElse("No name!"));


        // Option.map(x -> null) -> Some(null)
        assertThrows(NullPointerException.class, () -> Option.of("Name")
                .map(s -> (String) null)
                .map(String::toLowerCase)
                .getOrElse("No name!"));

        // Use of flatMap is required so no unattended NULL will sneak in
        assertEquals("No name!", Option.of("Name")
                .flatMap(s -> Option.of((String) null))
                .map(String::toLowerCase)
                .getOrElse("No name!"));


    }
}