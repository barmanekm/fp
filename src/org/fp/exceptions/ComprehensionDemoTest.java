package org.fp.exceptions;

import com.sun.deploy.nativesandbox.comm.Response;
import io.vavr.Function5;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;
import sun.net.www.http.HttpClient;

import java.io.IOException;
import java.net.URL;

import static io.vavr.API.For;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComprehensionDemoTest {

    private static String combineGreeting(String greet1, String greet2, String name) {
        return String.format("%s %s, %s!", greet1, greet2, name);
    }

    private static Option<String> combineGreetingOptionals(Option<String> greetOpt1,
                                                           Option<String> greetOpt2, Option<String> greetOpt3) {
        return For(greetOpt1, greetOpt2, greetOpt3)
                .yield(ComprehensionDemoTest::combineGreeting)
                .toOption();
    }


    @Test
    public void comprehensionAndCombinationTest_ChristmasTree() {
        Try<Option<String>> to1 = Try.of(() -> Option.of("Hello"));
        Try<Option<String>> to2 = Try.of(() -> Option.of("world"));
        Try<Option<String>> to3 = Try.of(() -> Option.of("Michal"));
        Try<Option<String>> to4 = Try.failure(new IOException("Can't retrieve name!"));

        Try<Option<String>> greeting =
                to1.flatMap(o1 ->
                        to2.flatMap(o2 ->
                                to3.map(o3 ->
                                        o1.flatMap(oo1 ->
                                                o2.flatMap(oo2 ->
                                                        o3.map(oo3 -> combineGreeting(oo1, oo2, oo3))
                                                )
                                        )
                                )
                        )
                );


        assertEquals("Hello world, Michal!", greeting.get().get());

        greeting.onFailure(System.out::println)
                .onSuccess(o -> System.out.println(o.getOrElse("No data available!")));

    }

    @Test
    public void comprehensionAndCombinationTest_ForApiVavr() {
        Try<Option<String>> to1 = Try.of(() -> Option.of("Hello"));
        Try<Option<String>> to2 = Try.of(() -> Option.of("world"));
        Try<Option<String>> to3 = Try.of(() -> Option.of("Michal"));


        Try<Option<String>> greeting =
                For(to1, to2, to3)
                        .yield(ComprehensionDemoTest::combineGreetingOptionals)
                        .toTry();

        assertEquals("Hello world, Michal!", greeting.get().get());
    }

    @Test
    public void comprehensionAndCombinationTest_WhenAnyFailure() {
        Try<Option<String>> to1 = Try.of(() -> Option.of("Hello"));
        Try<Option<String>> to2 = Try.of(() -> Option.of("world"));
        Try<Option<String>> to3 = Try.of(() -> { throw new IOException("Can't retrieve name!"); });
        Try<Option<String>> to4 = Try.of(() -> Option.of("Anna"));

        Try<Option<String>> greetingFailure =
                For(to1, to2, to3)
                        .yield(ComprehensionDemoTest::combineGreetingOptionals)
                        .toTry();

        Try<Option<String>> greetingSuccess =
                For(to1, to2, to4)
                        .yield(ComprehensionDemoTest::combineGreetingOptionals)
                        .toTry();

        assertEquals(true, greetingFailure.isFailure());
        assertEquals(true, greetingSuccess.isSuccess());

        greetingSuccess.onFailure(System.out::println)
                .onSuccess(o -> System.out.println(o.getOrElse("No data available!")));


        greetingFailure.onFailure(System.out::println)
                .onSuccess(o -> System.out.println(o.getOrElse("No data available!")));
    }

    @Test
    public void OptionForApiTest() {
        Option<String> o1 = Option.of("Hello");
        Option<String> o2 = Option.of("World");
        Option<String> empty = Option.of(null);

        Option<String> result =
                o1.flatMap(oo1 ->
                        o2.map(oo2 -> oo1 + " " + oo2));

        Option<String> resultEmpty1 = empty.flatMap(oo1 -> o2.map(oo2 -> oo1 + " " + oo2));
        Option<String> resultEmpty2 = o1.flatMap(oo1 -> empty.map(oo2 -> oo1 + " " + oo2));
        Option<String> resultEmpty3 = empty.flatMap(oo1 -> empty.map(oo2 -> oo1 + " " + oo2));


        assertEquals("Hello World", result.get());
        assertEquals(true, resultEmpty1.isEmpty());
        assertEquals(true, resultEmpty2.isEmpty());
        assertEquals(true, resultEmpty3.isEmpty());

        assertEquals(result, For(o1, o2).yield((oo1, oo2) -> oo1 + " " + oo2).toOption());
        assertEquals(resultEmpty1, For(empty, o2).yield((oo1, oo2) -> oo1 + " " + oo2).toOption());
        assertEquals(resultEmpty2, For(o1, empty).yield((oo1, oo2) -> oo1 + " " + oo2).toOption());
        assertEquals(resultEmpty3, For(empty, empty).yield((oo1, oo2) -> oo1 + " " + oo2).toOption());
    }

    @Test
    public void TryForApiTest() {
        Try<String> t1 = Try.of(() -> "Hello");
        Try<String> t2 = Try.of(() -> "World");
        Try<String> failure = Try.of(() -> { throw new IOException(); });

        Try<String> result = t1.flatMap(tt1 -> t2.map(tt2 -> tt1 + " " + tt2));
        Try<String> resultEmpty1 = failure.flatMap(tt1 -> t2.map(tt2 -> tt1 + " " + tt2));
        Try<String> resultEmpty2 = t1.flatMap(tt1 -> failure.map(tt2 -> tt1 + " " + tt2));
        Try<String> resultEmpty3 = failure.flatMap(tt1 -> failure.map(tt2 -> tt1 + " " + tt2));

        assertEquals("Hello World", result.get());
        assertEquals(true, resultEmpty1.isFailure());
        assertEquals(true, resultEmpty2.isFailure());
        assertEquals(true, resultEmpty3.isFailure());


        // Not exactly equal. Exception is swallowed. Bug or by design...
        assertEquals(result, For(t1, t2).yield((tt1, tt2) -> tt1 + " " + tt2).toTry());
        assertEquals(resultEmpty1, For(failure, t2).yield((tt1, tt2) -> tt1 + " " + tt2).toTry());
        assertEquals(resultEmpty2, For(t1, failure).yield((tt1, tt2) -> tt1 + " " + tt2).toTry());
        assertEquals(resultEmpty3, For(failure, failure).yield((tt1, tt2) -> tt1 + " " + tt2).toTry());
    }

    static Option<String> toUpperCase(String str) {
        return str == null ? Option.none() : Option.some(str.toUpperCase());
    }

    @Test
    public void flatMapVsMap() {
        assertEquals(Option.of(Option.of("HELLO!")), Option.of("Hello!").map(ComprehensionDemoTest::toUpperCase));
        assertEquals(Option.of("HELLO!"), Option.of("Hello!").flatMap(ComprehensionDemoTest::toUpperCase));
    }

}
