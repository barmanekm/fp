package org.fp.exceptions;

import io.vavr.Function1;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.junit.jupiter.api.Test;


import static io.vavr.API.*;
import static org.fp.exceptions.ExceptionsDemo.*;
import static org.junit.jupiter.api.Assertions.*;

class ExceptionsDemoTest {


    @Test
    void divide10ByTest(){
        //System.out.println(divide10ByStringX_Unsafe(null));
        //System.out.println(divide10ByStringX_Unsafe(""));
        //System.out.println(divide10ByStringX_Unsafe("0"));
        System.out.println(divide10ByStringX_Unsafe("5"));

    }























    @Test
    void divide10ByPureBetterTest() {
     assertEquals(Integer.valueOf(40), divide10ByStringX_PureTry("5").map(x -> x * x).map(x -> x * 10).get());
     assertEquals(true, divide10ByStringX_PureTry("5").isSuccess());

     //success (Side Effect application)
     divide10ByStringX_PureTry("5").onSuccess(System.out::println);


     //failure (Side Effect application)
     divide10ByStringX_PureTry("0").onSuccess(System.out::println).onFailure(System.out::println);
     divide10ByStringX_PureTry(null).onSuccess(System.out::println).onFailure(System.out::println);
     divide10ByStringX_PureTry("ala").onSuccess(System.out::println).onFailure(System.out::println);

     assertEquals(true, divide10ByStringX_PureTry("0").isFailure());
     assertEquals(true, divide10ByStringX_PureTry("").isFailure());
     assertEquals(true, divide10ByStringX_PureTry(null).isFailure());
     assertEquals(true, divide10ByStringX_PureTry(null).map(x -> x * x).isFailure());
    }

    @Test
    void divideLiftingDemo() {

        Function1<String, Integer> unsafe10Division = ExceptionsDemo::divide10ByStringX_Unsafe;
        Function1<String, Try<Integer>> safe10Division = Function1.liftTry(unsafe10Division);

        safe10Division.apply("5").onSuccess(System.out::println).onFailure(System.out::println);
        safe10Division.apply("0").onSuccess(System.out::println).onFailure(System.out::println);
    }
}