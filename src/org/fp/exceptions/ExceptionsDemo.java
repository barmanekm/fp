package org.fp.exceptions;

import io.vavr.control.Option;
import io.vavr.control.Try;

public class ExceptionsDemo {


    // String can be null
    // String can be not parsable to int
    // String can be parsed to 0
    public static Integer divide10ByStringX_Unsafe(String x) {
            Integer divider = Integer.parseInt(x);
            return 10 / divider;
    }




    public static Integer divide10ByStringX_Nullabe(String x) {
        try {
            Integer divider = Integer.parseInt(x);
            return 10 / divider;
        } catch (NumberFormatException | ArithmeticException e) {
            //log the stuff
            //throw e;
            return null;
        }
    }





    //Pure function
    public static Option<Integer> divide10ByStringX_PureOption(String x) {
        try {
            Integer divider = Integer.parseInt(x);
            return Option.of(10 / divider);
        } catch (NumberFormatException | ArithmeticException e) {
            return Option.none();
        }
    }







    //Pure function which do not swallow exception information
    public static Try<Integer> divide10ByStringX_PureTry(String x) {
        try {
            Integer divider = Integer.parseInt(x);
            return Try.success(10 / divider);
        } catch (NumberFormatException | ArithmeticException e) {
            return Try.failure(e);
        }
    }





}
