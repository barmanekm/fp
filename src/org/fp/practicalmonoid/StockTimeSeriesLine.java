package org.fp.practicalmonoid;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

public final class StockTimeSeriesLine {

    private final String symbol;
    private final Instant date;
    private final BigDecimal openPrice;
    private final BigDecimal highPrice;
    private final BigDecimal lowPrice;
    private final BigDecimal closePrice;
    private final Integer volume;


    private StockTimeSeriesLine(String symbol, Instant date, BigDecimal openPrice, BigDecimal highPrice,
                                BigDecimal lowPrice, BigDecimal closePrice, Integer volume) {
        this.symbol = symbol;
        this.date = date;
        this.openPrice = openPrice;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.closePrice = closePrice;
        this.volume = volume;
    }


    static StockTimeSeriesLine of(String symbol, Instant date, BigDecimal openPrice, BigDecimal highPrice,
                           BigDecimal lowPrice, BigDecimal closePrice, Integer volume) {
        return new StockTimeSeriesLine(symbol, date, openPrice, highPrice, lowPrice, closePrice, volume);
    }

    static Comparator<StockTimeSeriesLine> byDate = (u, v) -> u.getDate().isBefore(v.getDate()) ? 1 : 0;

    public String getSymbol() {
        return symbol;
    }

    public Instant getDate() {
        return date;
    }

    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    public BigDecimal getHighPrice() {
        return highPrice;
    }

    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    public BigDecimal getClosePrice() {
        return closePrice;
    }

    public Integer getVolume() {
        return volume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockTimeSeriesLine that = (StockTimeSeriesLine) o;
        return Objects.equals(symbol, that.symbol) &&
                Objects.equals(date, that.date) &&
                Objects.equals(openPrice, that.openPrice) &&
                Objects.equals(highPrice, that.highPrice) &&
                Objects.equals(lowPrice, that.lowPrice) &&
                Objects.equals(closePrice, that.closePrice) &&
                Objects.equals(volume, that.volume);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, date, openPrice, highPrice, lowPrice, closePrice, volume);
    }

    @Override
    public String toString() {
        return "StockTimeSeriesLine{" +
                "symbol='" + symbol + '\'' +
                ", date=" + date +
                ", openPrice=" + openPrice +
                ", highPrice=" + highPrice +
                ", lowPrice=" + lowPrice +
                ", closePrice=" + closePrice +
                ", volume=" + volume +
                '}';
    }
}
