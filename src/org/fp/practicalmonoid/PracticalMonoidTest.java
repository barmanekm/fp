package org.fp.practicalmonoid;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class PracticalMonoidTest {


    @Test
    void PracticalMonoidDemo() {

        // Data for each period is returned from separate parallel service call i various order

        List<StockTimeSeriesLine> period1 = Arrays.asList(
                fromCsvHandled("2018-02-20,91.4750,93.0600,91.0100,92.7200,30323240"),
                fromCsvHandled("2018-02-16,92.4500,93.5000,91.8000,92.0000,30188535"),
                fromCsvHandled("2018-02-15,91.2100,92.7200,90.6200,92.6600,27407914")
        );

        List<StockTimeSeriesLine> period2 = Arrays.asList(
                fromCsvHandled("2018-02-14,88.5100,90.9900,88.4100,90.8100,34314101"),
                fromCsvHandled("2018-02-13,88.9300,90.0000,87.8000,89.8300,26200053"),
                fromCsvHandled("2018-02-12,88.7350,89.7800,87.9295,89.1300,35501368")
        );

        List<StockTimeSeriesLine> period3 = Arrays.asList(
                fromCsvHandled("2018-02-09,86.3000,88.9300,83.8300,88.1800,57408480"),
                fromCsvHandled("2018-02-08,89.7100,89.8750,84.7600,85.0100,47485202"),
                fromCsvHandled("2018-02-07,90.4900,91.7700,89.2000,89.6100,40340551")
        );

        // series of not sorted partial reports
        Report period2Report = getReport(period2);
        Report period1Report = getReport(period1);
        Report period3Report = getReport(period3);

        Report fullReport = Stream.of(period1Report, period2Report, period3Report)
                .reduce(Report.neutral(), Report.combine);

        System.out.println(fullReport);
    }

    private Report getReport(List<StockTimeSeriesLine> periodData) {
        return periodData
                .stream()
                .map(x -> new Report(Stream.of(x), StockTimeSeriesStats.fromLine(x)))
                .reduce(Report.neutral(), Report.combine)
                .sortLines(); // if one want to have price lines ordered
    }


    // CSV Helpers
    private StockTimeSeriesLine fromCsv(String csvString) throws ParseException {
        String[] data = csvString.split(",");

        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        Instant date = dateParser.parse(data[0]).toInstant();

        DecimalFormat decimalFormat = new DecimalFormat("00.0000");
        decimalFormat.setParseBigDecimal(true);
        BigDecimal open = (BigDecimal) decimalFormat.parse(data[1]);
        BigDecimal high = (BigDecimal) decimalFormat.parse(data[2]);
        BigDecimal low = (BigDecimal) decimalFormat.parse(data[3]);
        BigDecimal close = (BigDecimal) decimalFormat.parse(data[4]);
        Integer volume = Integer.parseInt(data[5]);

        return StockTimeSeriesLine.of("MSFT", date, open, high, low, close, volume);
    }

    private StockTimeSeriesLine fromCsvHandled(String csvString) {
        try {
            return fromCsv(csvString);
        } catch (Exception ex) {
            System.out.println("Parsing CSV error");
            return null;
        }
    }
}
