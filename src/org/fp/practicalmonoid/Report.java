package org.fp.practicalmonoid;

import java.util.function.BinaryOperator;

import java.util.function.Function;

import java.util.stream.Stream;

final class Report {
    final Stream<StockTimeSeriesLine> lines;
    final StockTimeSeriesStats stats;

    Report(Stream<StockTimeSeriesLine> lines, StockTimeSeriesStats stats) {
        this.lines = lines;
        this.stats = stats;
    }

    static Report sortLines(Report report) {
        return new Report(report.lines.sorted(StockTimeSeriesLine.byDate), report.stats);
    }

    Report sortLines() {
        return Report.sortLines(this);
    }

    static Report neutral() {
        return new Report(Stream.empty(), StockTimeSeriesStats.neutral());
    }

    static BinaryOperator<Report> combine = (x, y) -> new Report(
            Stream.of(x.lines, y.lines).flatMap(Function.identity()),
            x.stats.ap(y.stats)
    );

    @Override
    public String toString() {
        return lines.map(StockTimeSeriesLine::toString)
                .reduce("", (x, y) -> x + System.lineSeparator() + y)
                .concat(System.lineSeparator())
                .concat("==========================================")
                .concat(System.lineSeparator())
                .concat(stats.toString());
    }
}
