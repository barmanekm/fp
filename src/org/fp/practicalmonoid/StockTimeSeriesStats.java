package org.fp.practicalmonoid;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;
import java.util.function.BinaryOperator;

public final class StockTimeSeriesStats {
    private final int dayCount;
    private final Integer totalVolume;
    private final Instant startDate;
    private final Instant endDate;
    private final BigDecimal openPrice;
    private final BigDecimal highestPrice;
    private final BigDecimal lowestPrice;
    private final BigDecimal closePrice;


    private StockTimeSeriesStats(int dayCount, Integer totalVolume, Instant startDate, Instant endDate,
                                 BigDecimal openPrice, BigDecimal highestPrice, BigDecimal lowestPrice,
                                 BigDecimal closePrice) {
        this.dayCount = dayCount;
        this.totalVolume = totalVolume;
        this.startDate = startDate;
        this.endDate = endDate;
        this.openPrice = openPrice;
        this.highestPrice = highestPrice;
        this.lowestPrice = lowestPrice;
        this.closePrice = closePrice;
    }

     static StockTimeSeriesStats fromLine(StockTimeSeriesLine line) {
        return new StockTimeSeriesStats(
                1, line.getVolume(),
                line.getDate(), line.getDate(),
                line.getOpenPrice(),
                line.getHighPrice(), line.getLowPrice(),
                line.getClosePrice()
        );
    }

    static StockTimeSeriesStats neutral() {
        return new StockTimeSeriesStats(
                0, 0,
                Instant.MAX,
                Instant.MIN,
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(Double.MIN_VALUE),
                BigDecimal.valueOf(Double.MAX_VALUE),
                BigDecimal.valueOf(0)
        );
    }

    //This is simple but sufficient in most cases possible operatoin of combining two entities
    // More sophisticated versions could use optionals.
    static BinaryOperator<StockTimeSeriesStats> combine =
            (x, y) -> new StockTimeSeriesStats(
                    x.dayCount + y.dayCount, x.totalVolume + y.totalVolume,
                    x.startDate.isBefore(y.startDate) ? x.startDate : y.startDate,
                    x.endDate.isAfter(y.endDate) ? x.endDate : y.endDate,
                    x.startDate.isBefore(y.startDate) ? x.openPrice : y.openPrice,
                    x.highestPrice.max(y.highestPrice),
                    x.lowestPrice.min(y.lowestPrice),
                    x.endDate.isAfter(y.endDate) ? x.closePrice : y.closePrice
            );

    StockTimeSeriesStats ap(StockTimeSeriesStats that) {
        return StockTimeSeriesStats.combine.apply(this, that);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockTimeSeriesStats that = (StockTimeSeriesStats) o;
        return dayCount == that.dayCount &&
                Objects.equals(totalVolume, that.totalVolume) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(openPrice, that.openPrice) &&
                Objects.equals(highestPrice, that.highestPrice) &&
                Objects.equals(lowestPrice, that.lowestPrice) &&
                Objects.equals(closePrice, that.closePrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dayCount, totalVolume, startDate, endDate, openPrice, highestPrice, lowestPrice, closePrice);
    }

    @Override
    public String toString() {
        return "StockTimeSeriesStats{" +
                "dayCount=" + dayCount +
                ", totalVolume=" + totalVolume +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", openPrice=" + openPrice +
                ", highestPrice=" + highestPrice +
                ", lowestPrice=" + lowestPrice +
                ", closePrice=" + closePrice +
                '}';
    }
}
