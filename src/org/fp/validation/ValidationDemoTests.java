package org.fp.validation;

import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;

import static org.fp.validation.ValidationDemo.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidationDemoTests {

    private final PersonValidator personValidator = new PersonValidator();

    private static String concatenateErrors(Seq<String> s) {
        return s.intersperse(", ")
                .fold("", String::concat);
    }

    @Test
    public void createValidAndInvalidPerson() {

        Validation<Seq<String>, Person> valid = personValidator.validatePerson("John Doe", 30);
        Validation<Seq<String>, Person> invalid = personValidator.validatePerson("John? Doe!4", -1);

        assertEquals(true, valid.isValid());
        assertEquals(true, invalid.isInvalid());
    }


    @Test
    public void displayResults() {

        Validation<Seq<String>, Person> valid = personValidator.validatePerson("John Doe", 30);
        Validation<Seq<String>, Person> invalid = personValidator.validatePerson("John? Doe!4", -1);

        System.out.println(valid.fold(
                ValidationDemoTests::concatenateErrors,
                Person::toString));

        System.out.println(invalid.fold(
                ValidationDemoTests::concatenateErrors,
                Person::toString));


        // Other possibilities are available example:
        // For invalid:
        // e -> Try.failure(new InvalidPersonDataException(concatenateErrors))
    }

    @Test
    public void conversionToEither() {

        Validation<Seq<String>, Person> valid = personValidator.validatePerson("John Doe", 30);
        Validation<Seq<String>, Person> invalid = personValidator.validatePerson("John? Doe!4", -1);


        //Either projections

        assertEquals(Either.right("Person(John Doe, 30)"),
                valid.toEither()
                        .right()
                        .map(Person::toString)
                        .toEither());


        assertEquals(true,
                invalid.toEither()
                        .right()
                        .map(Person::toString)
                        .toEither()
                        .isLeft());
    }
}
