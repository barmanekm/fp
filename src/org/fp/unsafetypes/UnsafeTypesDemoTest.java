package org.fp.unsafetypes;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class UnsafeTypesDemoTest {
    @Test
    void unsafeShopTest() {

        Product toothPaste = new Product("Tooth paste", 1.5, 0.5);
        Product toothBrush = new Product("Tooth brush", 3.5, 0.3);
        List<OrderLine> order = Arrays.asList(new OrderLine(toothPaste, 2), new OrderLine(toothBrush, 3));

        double weight = order.stream().mapToDouble(OrderLine::getAmount).sum();
        double price = order.stream().mapToDouble(OrderLine::getWeight).sum();

        System.out.println(String.format("Total price: %s", price));
        System.out.println(String.format("Total weight: %s", weight));

    }
}
