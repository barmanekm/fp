package org.fp.stockdemo;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class StockBrokerDemoTest {

    static class StockBrokerImperative {
        public static Stock buyStock(Account account, Stock stock) {
            account.charge(stock.price);
            return stock;
        }
    }

    static class StockBroker {
        public static Purchase buyStock(final Account account, final Stock stock) {
            Payment payment = new Payment(account, stock.price);
            return new Purchase(Arrays.asList(stock), payment);
        }

        public static Purchase buyStocks(final Account account, Stock stock, final int quantity) {
            List<Stock> stocks = Collections.nCopies(quantity, new Stock(stock.symbol, stock.price));
            Payment payment = new Payment(account, quantity * stock.price);
            return new Purchase(stocks, payment);
        }
    }

    @Test
    void buyingStock() {
        Account account = new Account("Mike", "123");
        Stock stock = new Stock("GOOG", 20.00);
        Purchase purchase = StockBroker.buyStocks(account, stock, 5);
        assertEquals(100, purchase.payment.amount);
        assertEquals(account, purchase.payment.account);
        assertEquals(5, purchase.stocks.size());
    }

}
