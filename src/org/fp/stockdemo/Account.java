package org.fp.stockdemo;

import java.util.Objects;

final class Account {
    public Account(String owner, String number) {
        this.owner = owner;
        this.number = number;
    }

    private String owner;
    private String number;

    void charge(double amount) {
        /* Contact bank, check amount, charge amount */
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(owner, account.owner) &&
                Objects.equals(number, account.number);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, number);
    }

    @Override
    public String toString() {
        return "Account{" +
                "owner='" + owner + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
