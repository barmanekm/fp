package org.fp.stockdemo;

import java.util.ArrayList;
import java.util.List;

public class Purchase {
    public List<Stock> stocks;
    public Payment payment;


    public Purchase(List<Stock> stocks, Payment payment) {
        this.stocks = new ArrayList<>(stocks);
        this.payment = payment;
    }
}
