package org.fp.stockdemo;

public class Payment {
    public final Account account;
    public final double amount;

    public Payment(Account account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    public Payment combine(Payment payment) {
        if (account.equals(payment.account))
            return new Payment(account, amount + payment.amount);
        throw new IllegalStateException("Two different accounts");
    }

}
